#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
/*
    schelet pentru exercitiul 5
*/

int* arr;
int array_size;
int nr_threads;
void *status;

typedef struct{
    int start, end, id;
}Thread_arg;

void *f(void *arg){
    Thread_arg *argument = (Thread_arg*) arg;
    for(int i = argument->start; i < argument->end; i++)
        arr[i] += 100;
}

int main(int argc, char *argv[]) {
    if (argc < 2) {
        perror("Specificati dimensiunea array-ului\n");
        exit(-1);
    }

    array_size = atoi(argv[1]);

    if (argc < 3){
        perror("Specificati numarul de thread-uri\n");
        exit(-1);
    }

    nr_threads = atoi(argv[2]);

    arr = malloc(array_size * sizeof(int));
    for (int i = 0; i < array_size; i++) {
        arr[i] = i;
    }

    for (int i = 0; i < array_size; i++) {
        printf("%d", arr[i]);
        if (i != array_size - 1) {
            printf(" ");
        } else {
            printf("\n");
        }
    }

    Thread_arg **thread_arg = malloc(nr_threads * sizeof(Thread_arg));
    for(int i = 0; i < nr_threads; i++)
        thread_arg[i] = malloc(sizeof(Thread_arg));

    pthread_t threads[nr_threads];

    // TODO: aceasta operatie va fi paralelizata
    for(int id = 0; id < nr_threads; id++){
        int start = id * (double)array_size / nr_threads;
        int aux = (id + 1) * (double)array_size / nr_threads;
        int end = aux < array_size ? aux:array_size;

        thread_arg[id]->id = id;
        thread_arg[id]->start = start;
        thread_arg[id]->end = end;

        int r = pthread_create(&threads[id], NULL, f, (void *)thread_arg[id]);

        if (r) {
            printf("Eroare la asteptarea thread-ului %d\n", id);
            exit(-1);
        }
    }

    for (int id = 0; id < nr_threads; id++) {
        int r = pthread_join(threads[id], &status);

        if (r) {
            printf("Eroare la asteptarea thread-ului %d\n", id);
            exit(-1);
        }
    }



    for (int i = 0; i < array_size; i++) {
        printf("%d", arr[i]);
        if (i != array_size - 1) {
            printf(" ");
        } else {
            printf("\n");
        }
    }

  	pthread_exit(NULL);
}
